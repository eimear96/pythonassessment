import re

ptn2=re.compile("#.*")
macnumber=re.compile("([a-fA-F0-9]{2}[:|\-]?){6}")
ipaddress=re.compile("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[^0-9]")
ptn3=re.compile("\s+")

fh=open("host.real.txt","r")
outfh=open("host.real2.txt","w")
outfh1=open("nextext.txt","w")

#Remove all of the comments from the file
for line in fh:
    if re.search('^#',line):
        continue
    if re.search('#',line):
        line=ptn2.sub(" ",line)
    data=ptn3.split(line)
    outfh1.write(" ".join(data)+"\n")

#Extract all of the MAC addresses from the file

    MAC=macnumber.search(line)
    if MAC:
        print(MAC.group())

#Finally remove all the extraneous spaces and write the IP addresses to a new file.

    IP=ipaddress.search(line)
    if IP:
        print(IP.group())
    outfh.write(data[0]+"\n")
fh.close()
